//
//  ViewController.swift
//  Teacher
//
//  Created by Aakash Shah on 12/10/23.
//

import UIKit
import ALProgressView
import SnapKit
import AudioToolbox
import Charts

enum Segment: Int {
    case overall = 0
    case latest7
    
    var title: String {
        switch self {
        case .overall:
            return "Overall"
        case .latest7:
            return "Latest 7"
        }
    }
}

class ViewController: UIViewController {
    
    let chartView = LineChartView()
    
    @IBOutlet weak var chartContainer: UIStackView?
    @IBOutlet weak var constForYBottom: NSLayoutConstraint?
    @IBOutlet var yaxisLbls: [UILabel]?

    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var segmentedControl: HBSegmentedControl!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    private lazy var progressRing = ALProgressRing()
    private lazy var emojiLabel = UILabel()
    
    var moodData: [MoodDataModel]?
    
    let MAX_TO_BE_SHOWN_IN_CURRENT = 6
    lazy var maxX_Axis_Points = -1
    var lineChartMoodOrder: [HTEmotions] = [.angry, .sad, .normal, .happy]
    let MAX_MOOD = 4

    var pauseViberateDispatchWorkItem: DispatchWorkItem?
    var currentSelectedSegment: Segment {
        maxX_Axis_Points = segmentedControl.selectedIndex == 0 ? -1 : MAX_TO_BE_SHOWN_IN_CURRENT
        return segmentedControl.selectedIndex == 0 ? .overall : .latest7
    }
    
    var dataEntries = [ChartDataEntry]()
    var intervalSize: Double = 3
    var xValue: Double = 0

    var index = 0
    var model = [
        MoodDataModel(lectureID: "12345", userID: "12345", emotion: .angry, timeLine: "0"),
        MoodDataModel(lectureID: "12345", userID: "12345", emotion: .normal, timeLine: "5"),
        MoodDataModel(lectureID: "12345", userID: "12345", emotion: .sad, timeLine: "10"),
        MoodDataModel(lectureID: "12345", userID: "12345", emotion: .happy, timeLine: "15"),
        MoodDataModel(lectureID: "12345", userID: "12345", emotion: .happy, timeLine: "20"),
        MoodDataModel(lectureID: "12345", userID: "12345", emotion: .happy, timeLine: "25"),
        MoodDataModel(lectureID: "12345", userID: "12345", emotion: .sad, timeLine: "30"),
        MoodDataModel(lectureID: "12345", userID: "12345", emotion: .sad, timeLine: "35"),
        MoodDataModel(lectureID: "12345", userID: "12345", emotion: .sad, timeLine: "40"),
        MoodDataModel(lectureID: "12345", userID: "12345", emotion: .normal, timeLine: "45"),
        MoodDataModel(lectureID: "12345", userID: "12345", emotion: .normal, timeLine: "50"),
        MoodDataModel(lectureID: "12345", userID: "12345", emotion: .angry, timeLine: "55"),
        MoodDataModel(lectureID: "12345", userID: "12345", emotion: .normal, timeLine: "60"),
        MoodDataModel(lectureID: "12345", userID: "12345", emotion: .angry, timeLine: "65"),
        MoodDataModel(lectureID: "12345", userID: "12345", emotion: .angry, timeLine: "70")
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = "Overall sentiment of the class"
        segmentedControl.items = [Segment.overall.title, Segment.latest7.title]
        segmentedControl.borderColor = .clear
        segmentedControl.selectedLabelColor = .white
        segmentedControl.unselectedLabelColor = .red
        segmentedControl.backgroundColor = .lightGray
        segmentedControl.thumbColor = .black
        segmentedControl.selectedIndex = 0
        segmentedControl.addTarget(self, action: #selector(ViewController.segmentValueChanged(_:)), for: .valueChanged)
        
        progressRing.duration = 2
        progressRing.timingFunction = .easeOutExpo
        progressView.addSubview(progressRing)
        progressRing.translatesAutoresizingMaskIntoConstraints = false
        let attributes: [NSLayoutConstraint.Attribute] = [.top, .bottom, .right, .left]
        NSLayoutConstraint.activate(attributes.map {
            NSLayoutConstraint(item: progressRing, attribute: $0, relatedBy: .equal, toItem: progressRing.superview, attribute: $0, multiplier: 1, constant: 0)
        })
        
        progressRing.addSubview(emojiLabel)
        emojiLabel.text = HTEmotions.happy.emoji
        emojiLabel.font = .systemFont(ofSize: 80, weight: .regular)
        
        emojiLabel.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
        
        setDefaultProgressInSlidder()
        setChart()
        self.updateProgress()
        self.updateLineChart()
        
        self.moodData = PubNubManager.shared.getAverageEmotionPerInterval()

//        Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(didUpdatedChartView), userInfo: nil, repeats: true)

        PubNubManager.shared.subscribe()
        PubNubManager.shared.receiveData { [weak self] data in
            debugPrint("Mood \(data.last?.emotion.rawValue ?? "")")
            debugPrint("Mood \(data.last?.timeLine ?? "")")
            self?.moodData = PubNubManager.shared.getAverageEmotionPerInterval()
            self?.updateProgress()
            self?.updateLineChart()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(5), execute: {[weak self] in
            self?.headerView.addBottomShadow()
        })
    }
    
    @objc func didUpdatedChartView() {
        index += 1
        let _index =  index % 15
        var m = model[_index]
        m.timeLine = String(index * 3)
        PubNubManager.shared.receivedData.append(m)
        print("Mood added \(m)")
        self.moodData = PubNubManager.shared.getAverageEmotionPerInterval()
        self.updateProgress()
        self.updateLineChart()
    }
    
    func setChart() {
        chartContainer?.addArrangedSubview(chartView)
        populateData()
    }
    
    func populateData() {
        dataEntries.removeAll()
        setupChartData()
        setupInitialDataEntries()
    }
    
    func setupInitialDataEntries() {
        
        var start = 0
        let overAllMoodData = moodData ?? []
        if currentSelectedSegment == .latest7 {
            if overAllMoodData.count > MAX_TO_BE_SHOWN_IN_CURRENT {
                start = (overAllMoodData.count - 1) - MAX_TO_BE_SHOWN_IN_CURRENT
            }
            else {
                start = 0
            }
        }
        else {
            start = 0
        }
        for index in start..<overAllMoodData.count {
            let seconds = Int(Double(overAllMoodData[index].timeLine) ?? 0)
            let dataEntry = ChartDataEntry(x: Double(seconds),
                                              y: overAllMoodData[index].emotion.lineChartOrder)
            dataEntries.append(dataEntry)
            chartView.data?.appendEntry(dataEntry, toDataSet: 0)
        }
        chartView.notifyDataSetChanged()
        chartView.moveViewToX(0)
    }
    
    func setupChartData() {
        // 1
        let chartDataSet = LineChartDataSet(entries: dataEntries, label: "Emotion")
        chartDataSet.drawCirclesEnabled = false
        chartDataSet.setColor(.red)
        chartDataSet.mode = .horizontalBezier
        chartDataSet.lineWidth = 3
        chartDataSet.valueTextColor = UIColor.clear

        // 2
        let chartData = LineChartData(dataSet: chartDataSet)
        chartView.data = chartData
        chartView.dragYEnabled = true
        chartView.dragXEnabled = true
        chartView.xAxis.labelPosition = .bottom
        chartView.xAxis.axisMinLabels = 0
        if currentSelectedSegment == .latest7 {
            chartView.xAxis.axisMaxLabels = maxX_Axis_Points
        }
        else {
            chartView.xAxis.axisMaxLabels = 12000
        }
        chartView.leftAxis.axisMinimum = 0  // This is what you need
        chartView.leftAxis.axisMaximum = 3  // This is what you need
        chartView.leftAxis.labelCount = 3
        chartView.leftAxis.labelTextColor = UIColor.clear
        chartView.rightAxis.drawAxisLineEnabled = false
        chartView.rightAxis.drawGridLinesEnabled = false
        chartView.rightAxis.drawLabelsEnabled = false
    }
    
    func updateChartView(with newDataEntry: ChartDataEntry, dataEntries: inout [ChartDataEntry]) {
        // 1
        if let oldEntry = dataEntries.first,
            currentSelectedSegment == .latest7,
           dataEntries.count > maxX_Axis_Points {
            dataEntries.removeFirst()
            chartView.data?.removeEntry(oldEntry, dataSetIndex: 0)
        }
        
        // 2
        dataEntries.append(newDataEntry)
        chartView.data?.appendEntry(newDataEntry, toDataSet: 0)
            
        // 3
        self.chartView.notifyDataSetChanged()
        self.chartView.moveViewToX(newDataEntry.x)
        
        let index = Int(newDataEntry.y)
        if let yaxisLbls, yaxisLbls.count > index {
            let lbl = yaxisLbls[index]
            zoomLbl(lbl: lbl)
        }
    }
    
    @objc func segmentValueChanged(_ sender: AnyObject?){
        populateData()
        updateProgress()
        updateLineChart()
        if currentSelectedSegment == .overall{
            titleLabel.text = "Overall sentiment of the class"
        }else{
            titleLabel.text = "Current sentiment of the class"
        }
    }
    
    func updateLineChart() {
        
        let overAllMoodData = moodData ?? []
        guard let lastMood = overAllMoodData.last else {
            return
        }
//        print("Mood Get \(lastMood)")
        let seconds = Int(Double(lastMood.timeLine) ?? 0)
        let newDataEntry = ChartDataEntry(x: Double(seconds),
                                          y: lastMood.emotion.lineChartOrder)
        updateChartView(with: newDataEntry, dataEntries: &dataEntries)
        xValue = Double(seconds) + intervalSize
        constForYBottom?.constant = 20
    }
    
    func setDefaultProgressInSlidder() {
        let defaultEmotion = HTEmotions.angry
        let percentage = Float(defaultEmotion.slidderOrder) / Float(MAX_MOOD - 1)
        updateProgressEmoji(type: defaultEmotion)
        progressRing.setProgress(percentage, animated: true)
        progressRing.startColor = defaultEmotion.sliderColor
        vibrate(emotion: defaultEmotion)
    }
    
    func updateProgress(){
        if let overAllMoodData = moodData {
            if currentSelectedSegment == .overall {
                if let averageMood = PubNubManager.shared.getAverage(emotions: overAllMoodData) {
                    let moodTypeOrder = averageMood.emotion.slidderOrder
                    let percentage = Float(moodTypeOrder) / Float(MAX_MOOD - 1)
                    progressRing.setProgress(Float(percentage), animated: true)
                    updateProgressEmoji(type: averageMood.emotion)
                    vibrate(emotion: averageMood.emotion)
                    progressRing.startColor = averageMood.emotion.sliderColor
                }
                else {
                    setDefaultProgressInSlidder()
                }
            }
            else {
                if let currentMood = overAllMoodData.last {
                    let moodTypeOrder = currentMood.emotion.slidderOrder
                    let percentage = Float(moodTypeOrder) / Float(MAX_MOOD - 1)
                    progressRing.setProgress(Float(percentage), animated: true)
                    updateProgressEmoji(type: currentMood.emotion)
                    vibrate(emotion: currentMood.emotion)
                    progressRing.startColor = currentMood.emotion.sliderColor
                }
                else {
                    setDefaultProgressInSlidder()
                }
            }
        }
        else {
            setDefaultProgressInSlidder()
        }
    }
    
    func updateProgressEmoji(type: HTEmotions){
        emojiLabel.text = type.emoji
    }
    
    func vibrate(emotion: HTEmotions) {
        guard emotion == .angry else {
            pauseViberation(lbl: emojiLabel)
            return
        }
        
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        vibrate(lbl: emojiLabel)
    }
    
    func vibrate(lbl: UILabel) {
        var iconShake = CABasicAnimation()
        
        iconShake = CABasicAnimation(keyPath: "transform.rotation.z")
        
        iconShake.fromValue = -0.15
        
        iconShake.toValue = 0.1
        
        iconShake.autoreverses = true
        
        iconShake.duration = 0.25
        iconShake.speed = 4
        iconShake.repeatCount = Float.greatestFiniteMagnitude
        
        lbl.layer.add(iconShake, forKey: "iconShakeAnimation")
        lbl.transform = CGAffineTransform(scaleX: 2, y: 2)
        
        pauseViberateDispatchWorkItem?.cancel()
        pauseViberateDispatchWorkItem = DispatchWorkItem(block: {[weak self] in
            self?.pauseViberation(lbl: lbl)
        })
        if let pauseViberateDispatchWorkItem {
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(2000), execute: pauseViberateDispatchWorkItem)
        }
    }

    func pauseViberation(lbl: UILabel) {
        lbl.transform = CGAffineTransform(scaleX: 1, y: 1)
        lbl.layer.removeAllAnimations()
    }
    
    func zoomLbl(lbl: UILabel) {
        UIView.animateKeyframes(withDuration: 2.0,
                                delay: 0,
                                options: UIView.KeyframeAnimationOptions.calculationModeLinear,
                                animations: {
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1.0, animations: {
                lbl.transform = CGAffineTransform(scaleX: 2, y: 2)
                lbl.layoutIfNeeded()
            })
            UIView.addKeyframe(withRelativeStartTime: 0.1, relativeDuration: 1.0, animations: {
                lbl.transform = CGAffineTransform(scaleX: 1, y: 1)
                lbl.layoutIfNeeded()
            })
        },
        completion: nil)
    }
}

extension UIView {
    func addBottomShadow() {
        layer.masksToBounds = false
        layer.shadowRadius = 4
        layer.shadowOpacity = 1
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOffset = CGSize(width: 0 , height: 2)
        layer.shadowPath = UIBezierPath(rect: CGRect(x: 0,
                                                     y: bounds.maxY - layer.shadowRadius,
                                                     width: bounds.width,
                                                     height: layer.shadowRadius)).cgPath
    }
}
