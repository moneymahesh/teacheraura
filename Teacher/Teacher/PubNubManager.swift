//
//  PubNubManager.swift
//  PhysicsWallah
//
//  Created by Peeyush Shrivastava on 12/10/23.
//

import Foundation
import PubNub
import UIKit

final class PubNubManager {
    static let shared = PubNubManager()

    private var client: PubNub?
    private let channels = ["Test_Hackathon"]
    private let listener = SubscriptionListener(queue: .main)

    private var selfData = [MoodDataModel]()
    var receivedData = [MoodDataModel]()

    private init() { }

    func configure() {
        let config = PubNubConfiguration(
            publishKey: "pub-c-1c506235-4dd7-4ff5-a5cb-818560254e62",
            subscribeKey: "sub-c-97df1466-e99a-4d39-965b-1d64d17b4dfd",
            userId: "642d441a1dd3890019d12169abcd"
          )
        client = PubNub(configuration: config)
    }

    func subscribe() {
        client?.add(listener)
        client?.subscribe(to: channels, withPresence: true)
    }

    func publish(with data: MoodDataModel) {
        // Save Emit Model
        selfData.append(data)

        self.listener.didReceiveStatus = { status in
            switch status {
                case .success(let connection):
                    if connection == .connected {
                        self.client?.publish(channel: self.channels[0],
                                                             message: self.getMsgDict(from: data)) { result in
                            print(result.map { "Publish Response at \($0.timetokenDate)" })
                        }
                    }
                case .failure(let error):
                    print("Status Error: \(error.localizedDescription)")
            }
        }
    }

    func receiveData(callBack: @escaping(_ data: [MoodDataModel]) -> Void) {
        PubNubManager.shared.listener.didReceiveMessage = { message in
            guard let dict = message.payload.codableValue.rawValue as? [String: String] else { return }
            guard let userID = dict["userID"],
                  let lectureID = dict["lectureID"],
                  let timeLine = dict["timeLine"],
                  let emotion = dict["emotion"],
                  let emo = HTEmotions(rawValue: emotion) else { return }

            let data = MoodDataModel(lectureID: lectureID,
                                     userID: userID,
                                     emotion: emo,
                                     timeLine: timeLine)

            // Save Received Model
            self.receivedData.append(data)
            
            callBack(self.receivedData)
        }
    }

    private func getMsgDict(from data: MoodDataModel) -> [String: String] {
        var tempDict = [String: String]()
        tempDict["lectureID"] = data.lectureID
        tempDict["userID"] = data.userID
        tempDict["emotion"] = data.emotion.rawValue
        tempDict["timeLine"] = data.timeLine
        
        return tempDict
    }

    public func getAverageEmotionPerInterval() -> [MoodDataModel] {
        let emotionsDataDict = Dictionary(grouping: receivedData, by: {(Int(Double($0.timeLine) ?? 0))})
        var averageEmotionPerInterval = [MoodDataModel]()
        let keys = (emotionsDataDict.keys.map({(Int(Double($0)))})).sorted(by: {$0 < $1})
        for key in keys {
            if let emotions = emotionsDataDict[key], emotions.count > 0 {
                if let averageEmotion = getAverage(emotions: emotions) {
                    averageEmotionPerInterval.append(averageEmotion)
                }
                else {
                    let dummyValue = MoodDataModel(lectureID: emotions[0].lectureID,
                                                   userID: emotions[0].userID,
                                                   emotion: .normal,
                                                   timeLine: String(key))
                    averageEmotionPerInterval.append(dummyValue)
                }
            }
        }
        if averageEmotionPerInterval.count == 0 {
            let dummyValue = MoodDataModel(lectureID: "",
                                           userID: "",
                                           emotion: .angry,
                                           timeLine: "0")
        }
        return averageEmotionPerInterval
    }
    
    func getAverage(emotions: [MoodDataModel]) -> MoodDataModel? {
            
            guard let lectureId = emotions.first?.lectureID,
                  let userId = emotions.first?.userID,
                  let timeLine = emotions.last?.timeLine else {
                return nil
            }
            let groupedEmotion = Dictionary(grouping: emotions, by: {$0.emotion})
            
            let angry = groupedEmotion[.angry]?.count ?? 0
            let happy = groupedEmotion[.happy]?.count ?? 0
            let normal = groupedEmotion[.normal]?.count ?? 0
            let sad = groupedEmotion[.sad]?.count ?? 0

            let max = max(angry, happy, normal, sad)
            let emotionType: HTEmotions
            switch max {
            case normal:
                emotionType = .normal
                
            case happy:
                emotionType = .happy
                
            case sad:
                emotionType = .sad

            case angry:
                emotionType = .angry
                
            default:
                return nil
            }
            return MoodDataModel(lectureID: lectureId, userID: userId, emotion: emotionType, timeLine: timeLine)
        }
}

//MARK: - MoodDataModel
struct MoodDataModel {
    let lectureID: String
    let userID: String
    let emotion: HTEmotions
    var timeLine: String
}

//MARK: - ReceivedDataModel
struct ReceivedDataModel {
    let normalCount: Int
    let angryCount: Int
    let sadCount: Int
    let happyCount: Int
}

//MARK: - MoodCountModel
struct MoodCountModel {
    let type: HTEmotions
    let count: Int
}

//MARK: - HTEmotions
enum HTEmotions: String {
    case normal = "Normal"
    case angry = "Angry"
    case sad = "Sad"
    case happy = "Happy"
    
    var lineChartOrder: CGFloat {
        switch self {
        case .normal:
            return 2
        case .angry:
            return 0
        case .sad:
            return 1
        case .happy:
            return 3
        }
    }
    
    var slidderOrder: CGFloat {
        switch self {
        case .normal:
            return 2
        case .angry:
            return 0
        case .sad:
            return 1
        case .happy:
            return 3
        }
    }
    
    var emoji: String {
        switch self {
        case .normal:
            return "🙂"

        case .angry:
            return "😣"

        case .sad:
            return "😒"

        case .happy:
            return "😀"
        }
    }
    
    var sliderColor: UIColor {
        switch self {
        case .normal:
            return .yellow
            
        case .angry:
            return .red
            
        case .sad:
            return .orange
            
        case .happy:
            return .green
        }
    }
}
